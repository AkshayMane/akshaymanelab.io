---
title: Bankers Algorithm
date: 2016-09-27 18:50:43
tags: 
	- Java
	- Bankers Algorithm
categories:
	- Program
---


Write an program which will implement the Banker's Algorithm.


{% codeblock  %}
import java.util.*;
class BankersAlgo{
	private int nProcess,nResource,need[][],avail[],alloc[][],claim[][],exe[];
	Scanner sc = new Scanner(System.in);
	public void start(){
		System.out.println("Enter Number of Processes : ");
		nProcess = sc.nextInt();
		System.out.println("Enter Number of Resources : ");
		nResource = sc.nextInt();
		need = new int[nProcess][nResource];
		alloc = new int[nProcess][nResource];
		claim = new int[nProcess][nResource];
		avail = new int[nResource];
		exe = new int[nProcess];
		System.out.println("Enter allocated Vector : ");
		getMatrix(alloc);
		System.out.println("Enter Claimed Vector : ");
		getMatrix(claim);
		System.out.println("Enter Available Vector : ");
		for(int i = 0 ; i < nResource; i++){
			avail[i] = sc.nextInt();
		}
		needResources();
		System.out.println("Claimed Resources : ");
		printMatrix(claim);
		System.out.println("Allocated Resources : ");
		printMatrix(alloc);
		System.out.println("Need Resources : ");
		printMatrix(need);
		int k = 0;
		while(k<nProcess){
			for(int i = 0; i < nProcess; i++){
				if(check(i) == 1 && exe[i] != 1){
					for(int j = 0; j < nResource; j++){
						avail[j] = avail[j] - need[i][j] + claim[i][j];
					}
					System.out.println("Process : "+i);
					exe[i] = 1;
					k++;
					break;
				}
			}
		}
		if(k == nProcess){
			System.out.println("Safe State.....");
		}else{
			System.out.println("Not in Safe State.........");
		}
	}
	public void getMatrix(int[][] matrix){
		for(int i = 0; i < nProcess; i++){
			for(int j = 0; j < nResource; j++){
				matrix[i][j] = sc.nextInt();
			}
		}
	}
	public void printMatrix(int[][] matrix){
		for(int i = 0; i < nProcess; i++){
			System.out.print("P"+i+"\t");
			for (int j = 0;j < nResource; j++){
				System.out.print(matrix[i][j]+"\t");
			}
			System.out.println("\n");
		}
	}
	public void needResources(){		
		for(int i = 0; i < nProcess; i++){
			for(int j = 0; j < nResource; j++){
				need[i][j] = claim[i][j] - alloc[i][j];
			}
		}
	}
	public int check(int i){
		for(int j = 0; j < nResource; j++){
			if(avail[j]<need[i][j]){
				return 0;
			}
		}
		return 1;
	}
	public static void main(String args[]){
		new BankersAlgo().start();	
	}
}
{% endcodeblock %}


Steps :
{% codeblock %}
javac BankersAlgo.java
java BankersAlgo	
{% endcodeblock %}


Output
![](/images/bankersalgo.png)

