---
title: TCP Client-Server Program In Java  
date: 2017-02-20 17:39:45
tags: 
	- Java
	- Client-Server Programs
	- TCP Client-Server Program
categories: Program
---


To Implement TCP Client-Server Program in JAVA
Server program save as "TCPServer.java"

{% codeblock %}
import java.io.*;
import java.net.*;
public class TCPServer{
	public static void main(String[] args) throws Exception{
		int i;
		ServerSocket sersock = new ServerSocket(3000);
		System.out.println("Server  ready for chatting");
		Socket sock = sersock.accept( );                     
		BufferedReader keyRead = new BufferedReader(new InputStreamReader(System.in));
		OutputStream ostream = sock.getOutputStream(); 
		PrintWriter pwrite = new PrintWriter(ostream, true);
		InputStream istream = sock.getInputStream();
		BufferedReader receiveRead = new BufferedReader(new InputStreamReader(istream));
		String receiveMessage, sendMessage;               
		while(true){
			if((receiveMessage = receiveRead.readLine()) != null) {
				System.out.print("Client : ");
				System.out.println(receiveMessage);  
				System.out.print("Server : ");       
				sendMessage = keyRead.readLine();
				pwrite.println(sendMessage);
				System.out.flush(); 
			}
		}             
	}                    
}
{% endcodeblock %}


Client Program save as "TCPClient.java"

{% codeblock %}
import java.io.*;
import java.net.*;
public class TCPClient
{
	public static void main(String[] args) throws Exception
	{
		Socket sock = new Socket("127.0.0.1", 3000);
		BufferedReader keyRead = new BufferedReader(new InputStreamReader(System.in));
		OutputStream ostream = sock.getOutputStream(); 
		PrintWriter pwrite = new PrintWriter(ostream, true);
		InputStream istream = sock.getInputStream();
		BufferedReader receiveRead = new BufferedReader(new InputStreamReader(istream));
		System.out.println("---------Start the chitchat---------");
		String receiveMessage, sendMessage; 
		while(true){
			System.out.print("Client : ");
			sendMessage = keyRead.readLine();
			pwrite.println(sendMessage);
			if((receiveMessage = receiveRead.readLine()) != null){
				System.out.print("Server : ");
				System.out.println(receiveMessage);	
				System.out.flush(); 
			}
		}

	}               
}
{% endcodeblock %}
Steps :
1. First run server program then client program (both the program must be run on two different command prompt)
2. Start chatting with server
3. Observe the output
	Server command prompt	
![](/images/TCPServer.png)
	Client command prompt
![](/images/TCPClient.png)