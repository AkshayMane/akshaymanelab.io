---
title: Scaling in Java 
date: 2016-09-27 17:32:40
tags: 
	- Computer Graphics
	- CG
	- Scaling
	- Java
	- Transformation
categories :
	- Program
---


To implement Scaling in Java.
{% codeblock %}
import java.applet.*;
import java.awt.*;
import java.util.*;

/*
<applet code="Scaling" height="500" width="500"></applet>
*/

public class Scaling extends Applet{
	public void paint(Graphics g){
		int P[][],pDash[][];
		int tx,ty,n;
		int T[][] = new int[3][3];
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter No. of points : ");
		n = sc.nextInt();
		P = new int[n][3];
		pDash = new int[n][3];
		System.out.println("Enter co-ordinates (X,Y) : ");
		for(int i = 0; i < n; i++){
			for(int j = 0; j < 3; j++){
				if(j == 2)
					P[i][j] = 1;
				else
					P[i][j] = sc.nextInt();
		
				//Scalling Transformation Matrix

				if(i == j)
					T[i][j] = 1;
				else
					T[i][j] = 0;
				
			}
		}
		
		System.out.println("Scale on X-axis by units :  ");
		tx = sc.nextInt();
		System.out.println("Scale on Y-axis by units :  ");
		ty = sc.nextInt();
		T[0][0] = tx;
		T[1][1] = ty;
		
		
		for(int i = 0; i < n ; i++){
			for(int j = 0; j < 3; j++){
				System.out.print(" "+P[i][j]);
			}
				System.out.println();
		}

		for(int i = 0; i < n ; i++){
			for(int j = 0; j < 3; j++){
				System.out.print(" "+T[i][j]);
			}
				System.out.println();
		}

		for(int i = 0; i < n; i++){
			for(int j = 0; j < 3; j++){
					pDash[i][j] = 0;
				for(int k = 0; k < 3; k++){
					pDash[i][j] += P[i][k] * T[k][j];
				}
				System.out.print(" "+pDash[i][j]);
			}
			System.out.println();
		}
		
		g.drawLine(P[0][0],P[0][1],P[1][0],P[1][1]);
		g.drawLine(P[2][0],P[2][1],P[1][0],P[1][1]);
		g.drawLine(P[0][0],P[0][1],P[2][0],P[2][1]);
		
		g.drawLine(pDash[0][0],pDash[0][1],pDash[1][0],pDash[1][1]);
		g.drawLine(pDash[2][0],pDash[2][1],pDash[1][0],pDash[1][1]);
		g.drawLine(pDash[0][0],pDash[0][1],pDash[2][0],pDash[2][1]);
	}
}
{% endcodeblock %}

Steps For execution
![](/images/Scaling.png)

Output
![](/images/Scalingoutput.png)

