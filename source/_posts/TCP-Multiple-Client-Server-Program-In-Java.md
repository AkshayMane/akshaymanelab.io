---
title: TCP Multiple Client-Server Program In Java
date: 2016-09-30 18:32:45
tags: 
	- Java
	- Client-Server Programs
	- TCP Client-Server Program
categories: Program
---
Write the program that connects multiple clients to server through sockets.
a. The server passes a number to the clients.
b. Each client (allotted a specific task) uses the number as input.


Server program save as "Server.java"

{% codeblock %}
import java.io.*;
import java.net.*;
public class Server {
 
    static final int PORT = 3000;
 
    public static void main(String args[]) {
        ServerSocket serverSocket = null;
        Socket socket = null;
 
        try {
            serverSocket = new ServerSocket(PORT);
        } catch (IOException e) {
            e.printStackTrace();
 
        }
        while (true) {
            try {
                socket = serverSocket.accept();
            } catch (IOException e) {
                System.out.println("I/O error: " + e);
            }
            // new threa for a client
            new EchoThread(socket).start();
        }
    }
}
 class EchoThread extends Thread {
    protected Socket socket;
    static int count = 1;
    protected int clientNo;
    public EchoThread(Socket clientSocket) {
        this.socket = clientSocket;
        this.clientNo = count;
        count = count + 1;
    }
 
    public void run() {
        InputStream inp = null;
        BufferedReader brinp = null;
        OutputStream out = null;
        PrintWriter pwrite = null;
        BufferedReader keyRead = null;
        try {
            keyRead = new BufferedReader(new InputStreamReader(System.in));
            out = socket.getOutputStream();
            pwrite = new PrintWriter(out, true);
            inp = socket.getInputStream();
            brinp = new BufferedReader(new InputStreamReader(inp));
        } catch (IOException e) {
            return;
        }
        String line;	
            try {
	            line = keyRead.readLine();
	            pwrite.println(line);
                line = brinp.readLine();
                if ((line == null) || line.equalsIgnoreCase("QUIT")) {
                    socket.close();
                    return;
                } else {
                    System.out.println("Client "+clientNo+" : "+line);
                    out.flush();
                }
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }
    }
}

{% endcodeblock %}


First Client Program save as "Client1.java"

{% codeblock %}
import java.io.*;
import java.net.*;
public class Client1
{
	public static void main(String[] args) throws Exception
	{
		String reverse="",str;
		Socket sock = new Socket("127.0.0.1", 3000);
		BufferedReader keyRead = new BufferedReader(new InputStreamReader(System.in));
		OutputStream ostream = sock.getOutputStream(); 
		PrintWriter pwrite = new PrintWriter(ostream, true);
		InputStream istream = sock.getInputStream();
		BufferedReader receiveRead = new BufferedReader(new InputStreamReader(istream));
		System.out.println("Start the chitchat");
		String receiveMessage,sendMessage = "please enter another number";
		int number = 0;
		//pwrite.println("Enter Number");
		if((receiveMessage = receiveRead.readLine()) != null)
			System.out.println(receiveMessage);
		number = Integer.parseInt(receiveMessage);
		if(number != 0){
			if(number%2 == 0){
				sendMessage =receiveMessage+" <- Even Number";
			}else{
				sendMessage = receiveMessage+" <- Odd Number";
			}
		}
		System.out.println(sendMessage);
		pwrite.println(sendMessage);
	}               
}
{% endcodeblock %}



Second Client Program save as "Client2.java"

{% codeblock %}
import java.io.*;
import java.net.*;
public class Client2
{
	public static void main(String[] args) throws Exception
	{
		String reverse="",str;
		Socket sock = new Socket("127.0.0.1", 3000);
		BufferedReader keyRead = new BufferedReader(new InputStreamReader(System.in));
		OutputStream ostream = sock.getOutputStream(); 
		PrintWriter pwrite = new PrintWriter(ostream, true);
		InputStream istream = sock.getInputStream();
		BufferedReader receiveRead = new BufferedReader(new InputStreamReader(istream));
		System.out.println("Start the chitchat");
		String receiveMessage,sendMessage = "please enter another number";
		int number = 0,m,flag = 0;
		//pwrite.println("Enter Number");
		if((receiveMessage = receiveRead.readLine()) != null)
			System.out.println(receiveMessage);
		number = Integer.parseInt(receiveMessage);
		m=number/2;    
		for(int i = 2; i <= m; i++){    
		   if(number%i==0){    
		   	sendMessage = number+" <- Number is not prime";
		   System.out.println(sendMessage);    
		   flag=1;    
		   break;    
		   }    
		  }    
		  if(flag==0){
		  	sendMessage=number+" <- Number is prime";
		  System.out.println(sendMessage);    
  			}
		//System.out.println(sendMessage);
		pwrite.println(sendMessage);
	}               
}s
{% endcodeblock %}
Steps :
1. First run server program then client programs (all the program must be run on  different command prompt)
2. Enter one number on each client command prompt 
	client1 - weather the number is Even or odd
	client2 - weather the number is prime or not
3. Observe the output
	Server command prompt	
![](/images/3BServer.png)
	Client1 command prompt
![](/images/3BClient1.png)
	Client2 command prompt
![](/images/3BClient2.png)