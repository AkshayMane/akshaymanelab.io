---
title: Reverse Number Using Shell Script
date: 2016-09-10 19:54:33
tags: 
	- Shell Script
	- Reverse Number
categories:
	- Linux Programming
---

Shell script defined as:
"Shell Script is series of command written in plain text file. Shell script is just like batch file is MS-DOS but have more power than the MS-DOS batch file."

Write Shell Script to print reverse of number.

{% codeblock %}
echo "Enter a Number:"
read a
rev=0
sd=0
or=$a
while [ $a -gt 0 ]
do
        sd=`expr $a % 10`
        temp=`expr $rev \* 10`
        rev=`expr $temp + $sd`
        a=`expr $a / 10`
done 
echo "Reverse of $or is $rev"
{% endcodeblock %}
Output
![](/images/reverse.png)