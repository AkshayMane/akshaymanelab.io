---
title: Reflection in Java 
date: 2016-09-26 17:32:40
tags: 
	- Computer Graphics
	- CG
	- Reflection
	- Java
	- Transformation
categories :
	- Program
---


To implement Reflection in Java.
{% codeblock %}
import java.applet.*;
import java.awt.*;
import java.util.*;
/*
<applet code="Reflection" height="500" width="500"></applet>
*/

public class Reflection extends Applet{
	public void paint(Graphics g){
		int P[][],pDash[][];
		int n,choice;
		int T[][] = new int[3][3];
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter No. of points : ");
		n = sc.nextInt();
		P = new int[n][3];
		pDash = new int[n][3];
		System.out.println("Enter co-ordinates (X,Y) : ");
		for(int i = 0; i < n; i++){
			for(int j = 0; j < 3; j++){
				if(j == 2)
					P[i][j] = 1;
				else
					P[i][j] = sc.nextInt();
		
				//Scalling Transformation Matrix

				T[i][j] = 0;
			
			}
		}
		System.out.println("Enter your choice : \n1. About X-axis\n2. About Y-axis\n3. About Origin\n4. y = X\n5. Y = -X\n");
		choice = sc.nextInt();
		switch(choice){
			case 1:
				T[0][0] = -1;
				T[1][1] = 1;
				T[2][2] = 1;
				break;
			case 2:
				T[0][0] = 1;
				T[1][1] = -1;
				T[2][2] = 1;
				break;
			case 3:
				T[0][0] = -1;
				T[1][1] = -1;
				T[2][2] = 1;
				break;
			case 4:
				T[0][1] = 1;
				T[1][0] = 1;
				T[2][2] = 1;
				break;
			case 5:
				T[0][1] = -1;
				T[1][0] = -1;
				T[2][2] = 1;
				break;
			default :	
				System.out.println("Invalid Number");
				break;
		}
		
		for(int i = 0; i < n ; i++){
			for(int j = 0; j < 3; j++){
				System.out.print(" "+P[i][j]);
			}
				System.out.println();
		}

		for(int i = 0; i < n ; i++){
			for(int j = 0; j < 3; j++){
				System.out.print(" "+T[i][j]);
			}
				System.out.println();
		}

		for(int i = 0; i < n; i++){
			for(int j = 0; j < 3; j++){
					pDash[i][j] = 0;
				for(int k = 0; k < 3; k++){
					pDash[i][j] += P[i][k] * T[k][j];
				}
				System.out.print(" "+pDash[i][j]);
			}
			System.out.println();
		}
		
		g.drawLine(P[0][0],P[0][1],P[1][0],P[1][1]);
		g.drawLine(P[2][0],P[2][1],P[1][0],P[1][1]);
		g.drawLine(P[0][0],P[0][1],P[2][0],P[2][1]);
		
		g.drawLine(pDash[0][0],pDash[0][1],pDash[1][0],pDash[1][1]);
		g.drawLine(pDash[2][0],pDash[2][1],pDash[1][0],pDash[1][1]);
		g.drawLine(pDash[0][0],pDash[0][1],pDash[2][0],pDash[2][1]);
	}
}
{% endcodeblock %}

Steps For execution
![](/images/Reflection.png)

Output
![](/images/Reflectionoutput.png)

