---
title: Digital Signature Using RSA Algorithm
date: 2017-02-12 18:32:45
tags: 
    - Java
    - RSA
categories: Program
---


Implement Digital signature scheme using RSA.
{% codeblock %}
import java.security.MessageDigest;
import java.io.DataInputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Random;
public class RSAMD{
    private BigInteger p;
    private BigInteger q;
    private BigInteger N;
    private BigInteger phi;
    private BigInteger e;
    private BigInteger d;
    private int        bitlength = 1024;
    private Random     r;
    public RSAMD(){
        r = new Random();
        p = BigInteger.probablePrime(bitlength, r);
        q = BigInteger.probablePrime(bitlength, r);
        N = p.multiply(q);
        phi = p.subtract(BigInteger.ONE).multiply(q.subtract(BigInteger.ONE));
        e = BigInteger.probablePrime(bitlength / 2, r);
        while (phi.gcd(e).compareTo(BigInteger.ONE) > 0 && e.compareTo(phi) < 0){
            e.add(BigInteger.ONE); 
        }
        d = e.modInverse(phi);
    }
    public BigInteger getPublicKey(){
        return e;
    }
    public BigInteger getPrivateKey(){
        return d;
    }
    public BigInteger getN(){
        return N;
    }
    public RSAMD(BigInteger e, BigInteger d, BigInteger N){
        this.e = e;
        this.d = d;
        this.N = N;
    }

    public String MDConversion(String password) throws Exception{
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(password.getBytes());
        byte byteData[] = md.digest();
        //convert the byte to hex format method 1
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < byteData.length; i++) {
         sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }
        //System.out.println("Digest(in hex format) : " + sb.toString());
        return sb.toString();
    }
    @SuppressWarnings("deprecation")
    public static void main(String[] args) throws Exception{
        RSAMD rsa = new RSAMD();
        DataInputStream in = new DataInputStream(System.in);
        String teststring,mdMessage;
        System.out.println("Enter the plain text:");
        teststring = in.readLine();
        System.out.println("Encrypting String: " + teststring);
        rsa.server(rsa.client(teststring,rsa),rsa,teststring);
        // decrypt
    }
    public byte[] client(String msg, RSAMD rsa) throws Exception{
        System.out.println("------------Client------------");
        String mdMessage = rsa.MDConversion(msg);
        System.out.println("String in Bytes: "
        + rsa.bytesToString(mdMessage.getBytes()));
        // encrypt
        byte[] encrypted = rsa.encrypt(mdMessage.getBytes());
        System.out.println("encrypted bytes : "+rsa.bytesToString(encrypted));
        return encrypted;
    } 
    public void server(byte[] encrypted, RSAMD rsa, String ogMsg) throws Exception{
        System.out.println("------------Server------------");
        byte[] decrypted = rsa.decrypt(encrypted);
        System.out.println("Decrypting Bytes: " + rsa.bytesToString(decrypted));
        System.out.println("Decrypted String   : "+new String(decrypted));
        System.out.println("MD of original msg : "+rsa.MDConversion(ogMsg));
    }
    public static String bytesToString(byte[] encrypted){
        String test = "";
        for (byte b : encrypted){
            test += Byte.toString(b);
        }
        return test;
    }
    // Encrypt message
    public byte[] encrypt(byte[] message){
        return (new BigInteger(message)).modPow(e, N).toByteArray();
    }
    // Decrypt message
    public byte[] decrypt(byte[] message){
        return (new BigInteger(message)).modPow(d, N).toByteArray();
    }
}
{% endcodeblock %}


Output
![](/images/rsamd.png)