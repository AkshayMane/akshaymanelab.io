---
title: Salami Attack
date: 2017-03-03 18:32:45
tags: 
	- Java
	- Salami Attack
categories: Program
---

To implement Salami Attack in JAVA
{% codeblock %}
import java.util.*;

class Person{
	String fname,lname;
	public static int id = 0;
	public Person(String fname, String lname){
		id += 1;
		this.fname = fname;
		this.lname = lname;
	}
	public String getFname(){
		return fname;
	}
	public String getLname(){
		return lname;
	}
	
}
class Bank{
	Person p;
	double rate = 5.36,ans;
	double amt = 0;
	int year;
	public  Bank(Person p,int year){
		this.p = p;
		this.year = year;
		amt = 1000;
	}
	public void deposite(int amt,Person p){
		if(p.id == this.p.id){
			this.amt += amt;
			System.out.println("Name : "+p.getFname()+" : Balance = "+this.amt);
		}else{
			System.out.println("Invalid user to access this account");
		}
	}
	public void withdrawal(int amt,Person p){
		if(p.id == this.p.id){
			this.amt -= amt;
			System.out.println("Name : "+p.getFname()+" : Balance = "+amt);
		}else{
			System.out.println("Invalid user to access this account");
		}
	}
	public double getAmt(){
		return amt;
	}
	public void FindInterest(){
		ans = (amt*year*rate)/100;
		amt += ans;
		System.out.println(p.fname+" : Simple interest = "+ans);
		Attacker a = new Attacker(p,this);
		a.hackInterest(ans);
	}
}

class Attacker{
	Bank b;
	static Bank my;
	Person p;
	double bal = 0;
	public Attacker(Person p, Bank b){
		this.p = p;
		this.b = b;
	}
	public Attacker(Bank b){
		this.my = b;
	}
	public void hackInterest(double ans){
		b.amt -= (ans-(int)ans);
		System.out.println("Hacked account balance("+p.fname+" "+p.lname+") : "+b.getAmt());
		my.amt += (ans - (int)ans);
		System.out.println("Balance (Hacker) "+my.getAmt());
	}
}


class Interest{
	public static void main(String args[]){
		Person p1 = new Person("Akshay","Mane");
		Bank b1 = new Bank(p1,6);
		b1.deposite(50000,p1);
		Person p2 = new Person("Prashant","Shivagan");
		Bank b2 = new Bank(p2,4);
		b2.deposite(38500,p2);
		Person p3 = new Person("Ruhi","Gujar");
		Bank b3 = new Bank(p3,2);
		b3.deposite(13000,p3);
		Attacker a = new Attacker(b3);
		b1.FindInterest();
		b2.FindInterest();		
	}
}
{% endcodeblock %}



Output
![](/images/salamiattack.png)