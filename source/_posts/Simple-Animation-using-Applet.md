---
title: Animation Using Applet
date: 2016-09-20 17:32:40
tags: 
	- Computer Graphics
	- CG
	- Animation
	- Java
categories :
	- Program
---
To implement Animation Using Applet.
{% codeblock %}
import java.applet.*;
import java.awt.*;
import java.util.*;
/*<applet code="Animation.class" height=500 width=500></applet>
*/
public class Animation extends Applet implements Runnable{
	int i,x=50,y=20,flag=1,x1=70,y1=110,x2=50,y2=200;
	public void run(){
		while(true){
			update();
		}
	}
	public void update(){
		try{
				repaint();
				if(flag <= 25 ){
					x = x + 1;
					y = y + 1;
					x2 = x2 + 1;
					y2 = y2 - 1;
				}else if(flag <= 50 && flag > 25){
					x = x + 1;
					y = y - 1;
					x2 = x2 + 1;
					y2 = y2 + 1;
				}else{
					flag = 0;
				}
				x1 = x1 + 1;
				flag = flag + 1;
					Thread.sleep(100);
			}catch(Exception e){
				System.out.println(e);
			}
	}
	public void start(){
		Thread t = new Thread(this);
		t.start();
	}
	public void paint(Graphics g){
		g.setColor(Color.red);
		g.fillOval(x,y,50,50);
		g.setColor(Color.yellow);
		g.fillOval(x1,y1,50,50);
		g.setColor(Color.green);
		g.fillOval(x2,y2,50,50);
	}
}
{% endcodeblock %}

Steps For execution
![](/images/Animation.png)

Output
![](/images/animation.gif)

