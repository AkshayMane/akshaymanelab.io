---
title: Simple C Program Hello World!
date: 2013-08-25 16:57:29
tags: C
categories: Programs
---
The simplest program in each language is Hello World!
The goal of this programis only print "Hello World!" as text on output window.
{% codeblock  %}
#include<stdio.h>

void main(){
	printf("Hello World!");
}

{% endcodeblock %}