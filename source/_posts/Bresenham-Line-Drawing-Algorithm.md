---
title: Bresenham's Line Drawing Algorithm
date: 2016-08-28 20:41:47
tags: 
	- Computer Graphics
	- CG
	- Bresenham’s Line Drawing Algorithm
	- Line Drawing Algorithm
	- Java
categories :
	- Program
---

To implement Bresenham’s Line Drawing Algorithm.
{% codeblock %}
import java.util.*;
import java.awt.*;
import java.applet.*;
/*
  <applet code="Bresenham" height=500 width=500></applet>
*/
public class Bresenham extends Applet{
	public void paint(Graphics g){
   		Scanner sc=new Scanner(System.in);
   		double x1,x2,y1,y2,dx,dy,x,y,e;
   		int i=1;
   		System.out.println("Enter Co-ordinates X1 Y1 X2 Y2 : ");
     		x1=sc.nextDouble();
      		y1=sc.nextDouble();
      		x2=sc.nextDouble();
      		y2=sc.nextDouble();
    		dx=Math.abs(x2-x1);
    		dy=Math.abs(y2-y1);
    		x=x1;
    		y=y1;
    		e=(2*dy)-dx;
     		for(i=1;i<=dx;i++)
     		{
			g.fillOval((int)x,(int)y,2,2);
          		while(e>=0)
        		{
            			y=y+1;
            			e=e-2*dx;
        		}
         		x=x+1;
         		e=e+(2*dy);
    		}
	}
}
{% endcodeblock %}

Steps For execution
![](/images/bresenham.png)

Output
![](/images/bresenhamoutput.png)
