---
title: Mid-point Circle Algorithm in Java 
date: 2016-09-02 17:32:40
tags: 
	- Computer Graphics
	- CG
	- Mid-point Circle Algorithm
	- Circle Generation Algorithm
	- Java
categories :
	- Program
---


To implement Mid-Point circle Algorithm.
{% codeblock %}
import java.util.*;
import java.awt.*;
import java.applet.*;
/* <applet code="MidPointCircle" height=200 width=200 ></applet>*/
public class MidPointCircle extends Applet{
	public void paint(Graphics g){
		int x,y,x1,y1;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the radius : ");
		x = sc.nextInt();
		System.out.println("Enter x and y : ");
		x1 = sc.nextInt();
		y1 = sc.nextInt();
		y = 0;
		double d = 1.25-x;
		while(x>=y){
			g.fillOval(x+x1,y+y1,2,2);
			g.fillOval(y+x1,x+y1,2,2);
			g.fillOval(-x+x1,y+y1,2,2);
			g.fillOval(-y+x1,x+y1,2,2);
			g.fillOval(x+x1,-y+y1,2,2);
			g.fillOval(y+x1,-x+y1,2,2);	
			g.fillOval(-x+x1,-y+y1,2,2);
			g.fillOval(-y+x1,-x+y1,2,2);
			y++;		
			if(d<0){
				d=d+2*y+1;
			}else{
				x--;
				d=d+2*(y-x+1);
			}
		}
	}
}
{% endcodeblock %}

Steps For execution
![](/images/midpoint.png)

Output
![](/images/midpointoutput.png)

