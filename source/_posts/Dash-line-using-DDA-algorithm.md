---
title: Dash Line Using DDA Algorithm
date: 2016-08-27 17:32:40
tags: 
	- Computer Graphics
	- CG
	- DDA Line Drawing Algorithm
	- Dash Line
	- Java
categories :
	- Program
---


To implement Dash Line using DDA Line Drawing Algorithm.
{% codeblock %}
import java.applet.*;
import java.awt.*;
import java.util.*;
/*
<applet code="DDAdash" height=200 width=200></applet>
*/
public class DDAdash extends Applet{
	public void paint(Graphics g){
		int x,y,dx,dy,len,i,flag = 3;
		int x1,x2,y1,y2;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter X1, Y1, X2, Y2 ");
		x1 = sc.nextInt();
		y1 = sc.nextInt();
		x2 = sc.nextInt();
		y2 = sc.nextInt();
		System.out.println("");
		x = x2-x1;
		y = y2-y1;
		if(Math.abs(x) > Math.abs(y)){
			len = Math.abs(x);
		}else{
			len = Math.abs(y);
		}
		dx = (int)((x2 - x1) / len) ;
		dy = (int)((y2 - y1) / len) ;
		
		if( dx > 0){
			x = (int)(x1 + 0.5);
		} else {
			x = (int)(x1 - 0.5);
		}
		if( dy > 0){
			y = (int)(y1 + 0.5);
		} else {
			y = (int)(y1 - 0.5);
		}
		
		i = 1;
		while( i <= len ){
			g.fillOval((int)x,(int)y,2,2);
			if(flag != 0){
				x = x + dx;
				y = y + dy;
				i++;
				flag--;
			}else{
				x = x + dx;
				y = y + dy;
				x = x + dx;
				y = y + dy;
				x = x + dx;
				y = y + dy;
				i = i+3;
				flag = 3;
			}
		}
	}
}
{% endcodeblock %}

Steps For execution
![](/images/DDAdashInput1.png)

Output
![](/images/DDAdashOutput1.png)

