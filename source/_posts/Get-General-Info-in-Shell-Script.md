---
title: Get General Info In Shell Script
date: 2016-09-22 19:54:33
tags: 
	- Shell Script
	- Get current date
	- Get current time
	- Get username
	- Get working directory
categories:
	- Linux Programming
---
Write a shell script to get current date, time, user name and current working directory.


{% codeblock %}
date_str="`date +%Y/%m/%d`"
time="`date +%H:%M:%S`"
wD=${PWD}
echo "Date - $date_str"
echo "Time - $time"
echo "Username - $USER"
echo "Working Directory - $wD"
$SHELL
{% endcodeblock %}
Output
![](/images/info.png)