---
title: TCP Client-Server Program In Java using Threads 
date: 2016-09-20 17:39:45
tags: 
	- Java
	- Client-Server Programs
	- TCP Client-Server Program
categories: Program
---
1. Write the program that connects client to server through sockets.
a. The server passes the string (two words) to client.
b. Client process splits the string.
c. Client spawns 2 child processes and passes one word to each child using pipe.
d. First child check the input is palindrome or not.
e. Second child converts the input to upper case. 


Server program save as "TCPServer.java"

{% codeblock %}
import java.io.*;
import java.net.*;
public class TCPServer{
	public static void main(String[] args) throws Exception{
		int i;
		ServerSocket sersock = new ServerSocket(3000);
		System.out.println("Server  ready for chatting");
		Socket sock = sersock.accept( );                     
		BufferedReader keyRead = new BufferedReader(new InputStreamReader(System.in));
		OutputStream ostream = sock.getOutputStream(); 
		PrintWriter pwrite = new PrintWriter(ostream, true);
		InputStream istream = sock.getInputStream();
		BufferedReader receiveRead = new BufferedReader(new InputStreamReader(istream));
		String receiveMessage, sendMessage;               
		if((receiveMessage = receiveRead.readLine()) != null) {
			System.out.println(receiveMessage);         
		}
		sendMessage = keyRead.readLine();
		pwrite.println(sendMessage);             
		System.out.flush(); 
	}                    
}

{% endcodeblock %}


Client Program save as "TCPClient.java"

{% codeblock %}
import java.io.*;
import java.net.*;
public class TCPClient
{
	public static void main(String[] args) throws Exception
	{
		String reverse="",str;
		Socket sock = new Socket("127.0.0.1", 3000);
		BufferedReader keyRead = new BufferedReader(new InputStreamReader(System.in));
		OutputStream ostream = sock.getOutputStream(); 
		PrintWriter pwrite = new PrintWriter(ostream, true);
		InputStream istream = sock.getInputStream();
		BufferedReader receiveRead = new BufferedReader(new InputStreamReader(istream));
		System.out.println("Start the chitchat, type and press Enter key");
		String receiveMessage, sendMessage; 
		pwrite.println("Enter two words : ");
		if((receiveMessage = receiveRead.readLine()) != null)
			System.out.println(receiveMessage);
 
		String[] splited = receiveMessage.split("\\s+");
 
		PipedInputStream pis = new PipedInputStream();
		PipedOutputStream pos = new PipedOutputStream();
		pos.connect(pis);	
 
		str=splited[0];
		byte[] b = str.getBytes(); 
		pos.write(b);
		pos.flush();	
 
		Runnable r1 = new Child1(pis,str.length());
		Thread t1 = new Thread(r1);
 
		str=splited[1];
		pos.write(str.getBytes());
		pos.flush();
 
		Runnable r2 = new Child2(pis,str.length());
		Thread t2 = new Thread(r2);
		t1.start();
		t1.join();
		t2.start();
		t2.join();
		System.out.flush();
	}               
} 
class Child1 implements Runnable{
	private String str;
	private String reverse = "";
	public Child1(PipedInputStream pis,int len){
		byte b[] = new byte[len];
		try{
			pis.read(b,0,len);
			this.str = new String(b);
		}catch(Exception e){
			System.out.println(e);
		}
	}
	public void run(){
		for ( int i = str.length() - 1; i >= 0; i-- )
			reverse = reverse + str.charAt(i);
		if (str.equals(reverse))
			System.out.println("Child1 : "+str+" <- string is a palindrome.");
		else
			System.out.println("Child2 : "+str+" <- Entered string is not a palindrome.");
	}
}
class Child2 implements Runnable{
	private String str;
	public Child2(PipedInputStream pis,int len){
		byte b[] = new byte[len];
		try{
			pis.read(b,0,len);
			this.str = new String(b);
		}catch(Exception e){
			System.out.println(e);
		}
	}
	public void run(){
		System.out.println("Child2 : "+str.toUpperCase());
	}
}

{% endcodeblock %}
Steps :
1. First run server program then client program (both the program must be run on two different command prompt)
2. Enter two string in Server command prompt
	a. First string for converting lower case to higher case
	b. Second string for checking weather it is palindrome or not
3. Observe the output
	Server command prompt	
![](/images/3AServer.png)
	Client command prompt
![](/images/3AClient.png)