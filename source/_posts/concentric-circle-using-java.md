---
title: Concentric Circle using Mid-point Circle Algorithm in Java 
date: 2016-09-03 17:32:40
tags: 
	- Computer Graphics
	- CG
	- Mid-point Circle Algorithm
	- Circle Generation Algorithm
	- Java
	- Concentric Circle
categories :
	- Program
---


To implement Mid-Point circle Algorithm.
{% codeblock %}
import java.util.*;
import java.awt.*;
import java.applet.*;
/*
<html>
	<body>
		<applet code="ConcentricCircle.class" height=500 width=500></applet>
	</body>
</html>

*/
public class ConcentricCircle extends Applet{
	public void paint(Graphics g){
		int x,y,x1,y1;
		double d;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the radius : ");
		x = sc.nextInt();
		System.out.println("Enter x and y : ");
		x1 = sc.nextInt();
		y1 = sc.nextInt();
		for(int i = 0; i < 3; i++){
			d = 1.25-x;
			y = 0;
			while(x>=y){
				g.fillOval(x+x1,y+y1,2,2);
				g.fillOval(y+x1,x+y1,2,2);
				g.fillOval(x+x1,-y+y1,2,2);
				g.fillOval(y+x1,-x+y1,2,2);
				g.fillOval(-x+x1,y+y1,2,2);
				g.fillOval(-y+x1,x+y1,2,2);
				g.fillOval(-x+x1,-y+y1,2,2);
				g.fillOval(-y+x1,-x+y1,2,2);
				y++;		
				if(d<0){
					d=d+2*y+1;
				}else{
					x--;
					d=d+2*(y-x+1);
				}
			}
			x = x + 20;
		}
	}
}
{% endcodeblock %}

Steps For execution
![](/images/concentriccircle.png)

Output
![](/images/concentriccircleoutput.png)

