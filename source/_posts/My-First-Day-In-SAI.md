---
title: My First Day In SAI
date: 2015-09-19 18:21:22
tags: 
- Sai Ashirwad Informatia
Categories :
- Experience
---

{% blockquote  %}
Whenever we join any course, On first day of the course anybody who teach us, they try to introduce about course which is new for us. But on my first day, My sir, {% link Rohan Sakhale http://www.rohansakhale.com %} taught about knowledge. He taught what is knowledge life cycle? How it’s work? He just passed knowledge whatever he learnt at {% link Rajesh Patkar Institute of Software Engineering http://www.rajeshpatkar.com/   %}
. He shared the thinking patterns with me and my friends, he taught four kinds of thinking patterns which are useful in future for developing any product in effective manner.
What is an Engineer? In SAI, Engineer is a person who developed something out of a given raw material into finished product in timely and cost effective manner.
He says with the views of Rajesh Patkar Sir in thinking patterns,
{% endblockquote %}

1.	Solution to a given problem is always Contextual.
2.	To achieve maximum knowledge we should be at lowest abstraction layer.
3.	Add more view to solve a given problem with best possible way.
4. 	Always limit thinking to a given problem.

{% blockquote %}
So this is my first day in SAI which is most valuable period for me, This is right place to develop your knowledge, I think after accomplish this course I will be able to make use of knowledge in right way to achieve my goals.
{% endblockquote %}