---
title: Calculate Area Using Shell Script
date: 2016-09-12 19:54:33
tags: 
	- Shell Script
	- Calculate area 
	- Area of Rectangle
	- Area of Triangle
	- Area of Circle
categories:
	- Linux Programming
---

Write a menu driven shell script to find out area of rectangle, circle, and triangle.
{% codeblock %}
clear
i="y"
while [ $i = "y" ]
do
	echo "Menu "
	echo "1. Area of Rectangle "
	echo "2. Area of Circle"
	echo "3. Area of Triangle"
	echo "4. Quit"
	echo "Enter ur Choice \c"
	read Choice
	case $Choice in
	  1) 
			echo "Enter the length and breadth of rectangle:"
			read leng
			read brea
			echo "Area of the rectangle is: `expr $leng \* $brea`"
		  ;;
	 2)	echo -n "Enter radius of circle:- "
			read r
			echo -n "Area of circle is :- $carea"
			carea= echo "3.14 * $r * $r" | bc
		 ;;
	3)
		echo -n "Please enter the base and height of the triangle : "
		read base 
		read height
		area=`expr "scale=2; 1/2*$base*$height"|bc`
		echo "area of the triangle = $area"
		;;
	4) 
		 echo "Exit......."
		 exit;;
	esac
	echo "Do u want to continue ?"
	read i
	if [ $i != "y" ]
	then
		 exit
	fi
done    
{% endcodeblock %}
Output
![](/images/area.png)