---
title: Multithreading In Java
date: 2016-09-15 18:30:32
tags: 
	- Multithreading 
	- Java
	- Thread
categories:
	- Program
---


Write the program which will find the sum of the first 300 numbers.
a. Create 3 threads which will do the following -
	 Thread 1 – Sum of numbers from 1 – 100
	 Thread 2 – Sum of numbers from 101 - 200
	 Thread 3 – Sum of numbers from 201 – 300
b. Find the total sum by adding the results of each thread 


{% codeblock %}
import java.util.*;
class AdditionThread implements Runnable{
	int a,b,sum = 0;	
	static int Total = 0;
	public AdditionThread(int a,int b){
		this.a = a;
		this.b = b;
	}
	public  void run(){
		synchronized(this){
			additionNum(a,b);
			Total += sum;
		}
	}
	public synchronized void additionNum(int a,int b){
		for(int i = a; i <= b; i++){
			sum += i;	
		}
		System.out.println("Sum of number from "+a+"-"+b+" = "+sum);
	}
	public static void printTotal(){
		System.out.println("Total Sum "+Total);
	}
	
}
class Addition{
	public static void main(String args[]){
		AdditionThread a1 = new AdditionThread(1,100);
		AdditionThread a2 = new AdditionThread(100,200);
		AdditionThread a3 = new AdditionThread(200,300);
		Thread t1 = new Thread(a1);	
		Thread t2 = new Thread(a2);		
		Thread t3 = new Thread(a3);
    			try{
			t1.start();
			t1.join();
			t2.start();
			t2.join();
			t3.start();
			t3.join();
			}catch(Exception e){
				System.out.println(e);
			}
		AdditionThread.printTotal();
 	}
}
{% endcodeblock %}


Steps :
{% codeblock %}
javac Addition.java
java Addition	
{% endcodeblock %}


Output
![](/images/addition.png)