---
date: 2017-06-19 19:06:16
---

<h2>About Akshay Mane</h2>
Mr. Akshay Mane is final year student studying at {% link Fr. Conceicao Rodrigues College of Engineering http://www.frcrce.ac.in FRCRCE %} in Information Technology branch. He will be the first member of his family who becomes Engineer. He completed his diploma in Information Technology from {% link Government Polytechnic, Bandra http://www.gpmumbai.ac.in %}. He is a rank holder in 10th standard in his school. He loves to do projects. His area of interest is broad. He has the ability to learn any thing about programming in quick session.
He started programming in 2015. The first project he did for the final year in diploma. He did several projects using web technology. He also participated in 1st Smart India Hackathon, 2017 where he did Non-Stop 36 hours code. He also plays wrestling. He has 3 state level certificates in wrestling.